<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/api', [ApiController::class, 'index']);
Route::post('/api/user_details',  [ApiController::class, 'userDetails']);
Route::post('/api/refresh_token',  [ApiController::class, 'refreshToken']);
Route::post('/api/revoke_token',  [ApiController::class, 'revokeToken']);
Route::post('/api/sign_up',  [ApiController::class, 'signUp']);
Route::post('/api/password_reset',  [ApiController::class, 'passwordReset']);
Route::post('/api/list_contacts',  [ApiController::class, 'listContacts']);
Route::post('/api/contact_privileges',  [ApiController::class, 'contactPrivileges']);
Route::post('/api/contact',  [ApiController::class, 'addContact']);
Route::post('/api/get_contacts_details',  [ApiController::class, 'getСontactsDetails']);
Route::post('/api/edit_concat',  [ApiController::class, 'editContact']);
Route::post('/api/list_all_portal_notifications',  [ApiController::class, 'listAllPortalNotifications']);
Route::post('/api/list_new_portal_notifications',  [ApiController::class, 'listNewPortalNotifications']);
Route::post('/api/acknowledge_notification',  [ApiController::class, 'acknowledgeNotification']);
Route::post('/api/account_balance',  [ApiController::class, 'accountBalance']);
Route::post('/api/list_invoices',  [ApiController::class, 'listInvoices']);
Route::post('/api/invoice_details',  [ApiController::class, 'invoiceDetails']);
Route::post('/api/payment_methods',  [ApiController::class, 'paymentMethods']);
Route::post('/api/payment_methods_fees',  [ApiController::class, 'paymentMethodsFees']);
Route::post('/api/list_services',  [ApiController::class, 'listServices']);
Route::post('/api/upgrade_options',  [ApiController::class, 'upgradeOptions']);
Route::post('/api/cancel_service',  [ApiController::class, 'cancelService']);
Route::post('/api/service_label',  [ApiController::class, 'serviceLabel']);
Route::post('/api/change_service_label',  [ApiController::class, 'changeServiceLabel']);
Route::post('/api/service_details',  [ApiController::class, 'serviceDetails']);
Route::post('/api/upgrade_request',  [ApiController::class, 'upgradeRequest']);
Route::post('/api/get_service_id_from_order_number',  [ApiController::class, 'getServiceIdFromOrderNumber']);
Route::post('/api/list_product_categories',  [ApiController::class, 'listProductCategories']);
Route::post('/api/list_products_in_category',  [ApiController::class, 'listProductsInCategory']);
Route::post('/api/get_product_configuration_details',  [ApiController::class, 'getProductConfigurationDetails']);
Route::post('/api/order_multiple_services',  [ApiController::class, 'orderMultipleServices']);
Route::post('/api/get_order_quote',  [ApiController::class, 'getOrderQuote']);
Route::post('/api/order_new_service',  [ApiController::class, 'orderNewService']);
Route::post('/api/get_available_vps_products',  [ApiController::class, 'getAvailableVPSProducts']);
Route::post('/api/order_addon',  [ApiController::class, 'orderAddon']);
Route::post('/api/list_all_addons',  [ApiController::class, 'listAllAddons']);
Route::post('/api/list_dns',  [ApiController::class, 'listDNS']);
Route::post('/api/add_dns_zone',  [ApiController::class, 'addDNSZone']);
Route::post('/api/get_dns_details',  [ApiController::class, 'getDNSDetails']);
Route::post('/api/remove_dns_zone',  [ApiController::class, 'removeDNSZone']);
Route::post('/api/add_dns_record',  [ApiController::class, 'addDNSRecord']);
Route::post('/api/edit_dns_record',  [ApiController::class, 'editDNSRecord']);
Route::post('/api/remove_dns_record',  [ApiController::class, 'removeDNSRecord']);
Route::post('/api/list_ssl_certificates',  [ApiController::class, 'listSSLCertificates']);
Route::post('/api/download_certificate',  [ApiController::class, 'downloadCertificate']);
Route::post('/api/list_available_certificates',  [ApiController::class, 'listAvailableCertificates']);
Route::post('/api/list_server_software_for_certificates',  [ApiController::class, 'listServerSoftwareForCertificates']);
Route::post('/api/list_all_servers',  [ApiController::class, 'listAllServers']);
Route::post('/api/server_details',  [ApiController::class, 'serverDetails']);
Route::post('/api/task_result',  [ApiController::class, 'taskResult']);
Route::post('/api/reboot',  [ApiController::class, 'reboot']);
Route::post('/api/reinstall',  [ApiController::class, 'reinstall']);
Route::post('/api/reset_password_server',  [ApiController::class, 'resetPasswordServer']);
Route::post('/api/launch_webConsole',  [ApiController::class, 'launchWebConsole']);
Route::post('/api/change_hostname',  [ApiController::class, 'changeHostname']);
Route::post('/api/change_prt_record',  [ApiController::class, 'changePTRRecord']);
Route::post('/api/flush_ip_tables',  [ApiController::class, 'flushIPTables']);
Route::post('/api/change_dns_servers',  [ApiController::class, 'changeDNSServers']);
Route::post('/api/available_os_list',  [ApiController::class, 'availableOSList']);
Route::post('/api/get_additional_ips',  [ApiController::class, 'getAdditionalIPs']);
Route::post('/api/get_usage_graphs',  [ApiController::class, 'getUsageGraphs']);
Route::post('/api/get_usage_history_server',  [ApiController::class, 'getUsageHistoryServer']);
Route::post('/api/get_available_init_scripts',  [ApiController::class, 'getAvailableInitScripts']);
Route::post('/api/list_vpn_servers',  [ApiController::class, 'listVPNServers']);
Route::post('/api/list_vpn_clients',  [ApiController::class, 'listVPNClients']);
Route::post('/api/vpn_config_template',  [ApiController::class, 'vpnConfigTemplate']);
Route::post('/api/login_details',  [ApiController::class, 'loginDetails']);
Route::post('/api/reset_password_vpn',  [ApiController::class, 'resetPasswordVPN']);
Route::post('/api/get_usage_history_vpn',  [ApiController::class, 'getUsageHistoryVPN']);
Route::post('/api/get_active_vpn_sessions',  [ApiController::class, 'getActiveVPNSessions']);
Route::post('/api/get_init_script_by_id',  [ApiController::class, 'getInitScriptByID']);

Route::get('/api/user_details', [ApiController::class, 'userDetails']);
Route::get('/api', [ApiController::class, 'user']);
