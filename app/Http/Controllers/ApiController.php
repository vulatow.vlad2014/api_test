<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
class ApiController extends BaseController
{
    public $response = [
      'isError' => false,
      'errorMessage' => '',
      'data' => [],
      'http_code' => null,
    ];
    public $authBase64 = false;

    public function __construct(Request $request){
       $this->authBase64 = ["Authorization: Basic " . base64_encode("{$request->input('login')}:{$request->input('password')}")];
    }
    public function __destruct(){
       echo json_encode($this->response);
    }
    public function sendRequest($data) {
        try{
          $url = $data['url'];
          $curl = curl_init($url);
          curl_setopt($curl, CURLOPT_URL, $url);
          if(isset($data['requestMethod'])){
            curl_setopt($curl, $data['requestMethod'], true);
          }
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HTTPHEADER, $this->authBase64);

          if(isset($data['postFields'])){
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data['postFields']);
          }
          $resp = curl_exec($curl);
          $this->response['http_code'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
          curl_close($curl);
          $response = (array)json_decode($resp);

          if(isset($response['error'])){
             $this->response['errorMessage'] = $response['error'];
             $this->response['isError'] = true;
          }else{
            $this->response['data'] = $response;
          }

        }catch(Exception $e){
          $this->response['errorMessage'] = $e->getMessage();
        }
    }

    // START CLIENTAREA
    public function userDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/details',
      ];
      self::sendRequest($data);
    }

    public function refreshToken(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/token',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => ["refresh_token" => $request->input('token')]
      ];
      self::sendRequest($data);
    }

    public function revokeToken(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/revoke',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => ["refresh_token" => $request->input('token')]
      ];
      self::sendRequest($data);
    }

    public function signUp(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/signup',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => ["refresh_token" => $request->input('token')]
      ];
      self::sendRequest($data);
    }

    public function updateUserDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/details',
              'requestMethod' => 'CURLOPT_PUT',
              'postFields' => [
                "type" => $request->input("typeValue"),
                "companyname" => $request->input("companynameValue"),
                "companyregistrationnumber" => $request->input("companyregistrationnumberValue"),
                "vateu" => $request->input("vateuValue"),
                "email" => $request->input("emailValue"),
                "firstname" => $request->input("firstnameValue"),
                "lastname" => $request->input("lastnameValue"),
                "country" => $request->input("countryValue"),
                "address1" => $request->input("address1Value"),
                "city" => $request->input("cityValue"),
                "state" => $request->input("stateValue"),
                "postcode" => $request->input("postcodeValue"),
                "phonenumber" => $request->input("phonenumberValue"),
                "emarketing" => $request->input("emarketingValue")
              ]
      ];
      self::sendRequest($data);
    }

    public function passwordReset(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/passwordreset',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => ["email" => $request->input('email')]
      ];
      self::sendRequest($data);
    }

    public function listContacts(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/contact',
      ];
      self::sendRequest($data);
    }

    public function contactPrivileges(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/contact/privileges',
      ];
      self::sendRequest($data);
    }

    public function addContact(Request $request)
    {
        $data = [
                'url' => 'https://billing.time4vps.com/api/contact',
                'requestMethod' => 'CURLOPT_POST',
                'postFields' => [
                  "password"=> $request->input("passwordValue"),
                  "privileges"=> $request->input("privilegesValue"),
                  "type"=> $request->input("typeValue"),
                  "companyname"=> $request->input("companynameValue"),
                  "companyregistrationnumber"=> $request->input("companyregistrationnumberValue"),
                  "vateu"=> $request->input("vateuValue"),
                  "email"=> $request->input("emailValue"),
                  "firstname"=> $request->input("firstnameValue"),
                  "lastname"=> $request->input("lastnameValue"),
                  "country"=> $request->input("countryValue"),
                  "address1"=> $request->input("address1Value"),
                  "city"=> $request->input("cityValue"),
                  "state"=> $request->input("stateValue"),
                  "postcode"=> $request->input("postcodeValue"),
                  "phonenumber"=> $request->input("phonenumberValue"),
                  "emarketing"=> $request->input("emarketingValue")
              ]
        ];
        self::sendRequest($data);
    }

    public function getСontactsDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/contact/' . $request->input('id'),
      ];
      self::sendRequest($data);
    }

    public function editContact(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/contact/' . $request->input('id'),
              'requestMethod' => 'CURLOPT_PUT',
              'postFields' => [
                "privileges" => $request->input("privilegesValue"),
                "type" => $request->input("typeValue"),
                "companyname" => $request->input("companynameValue"),
                "companyregistrationnumber" => $request->input("companyregistrationnumberValue"),
                "vateu" => $request->input("vateuValue"),
                "email" => $request->input("emailValue"),
                "firstname" => $request->input("firstnameValue"),
                "lastname" => $request->input("lastnameValue"),
                "country" => $request->input("countryValue"),
                "address1" => $request->input("address1Value"),
                "city" => $request->input("cityValue"),
                "state" => $request->input("stateValue"),
                "postcode" => $request->input("postcodeValue"),
                "phonenumber" => $request->input("phonenumberValue"),
                "emarketing" => $request->input("emarketingValue")
            ]
      ];
      self::sendRequest($data);
    }

    public function listAllPortalNotifications(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/notifications',
      ];
      self::sendRequest($data);
    }

    public function listNewPortalNotifications(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/notifications/new',
      ];
      self::sendRequest($data);
    }

    public function acknowledgeNotification(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/notifications/' . $request->input('id') . '/ack',
              'requestMethod' => 'CURLOPT_PUT',
      ];
      self::sendRequest($data);
    }
    // END CLIENTAREA

    // START BILLING
    public function accountBalance(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/balance',
      ];
      self::sendRequest($data);
    }

    public function listInvoices(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/invoice',
      ];
      self::sendRequest($data);
    }

    public function invoiceDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/invoice/' . $request->input('id'),
      ];
      self::sendRequest($data);
    }

    public function paymentMethods(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/payment',
      ];
      self::sendRequest($data);
    }

    public function paymentMethodsFees(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/payment/fees',
      ];
      self::sendRequest($data);
    }
    //END BILLING

    //START SERVICES
    public function listServices(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service',
      ];
      self::sendRequest($data);
    }

    public function listServiceMethods(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/'. $request->input('id') . '/methods',
      ];
      self::sendRequest($data);
    }

    public function upgradeOptions(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/'. $request->input('id') . '/upgrade',
      ];
      self::sendRequest($data);
    }

    public function cancelService(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/'. $request->input('id') . '/cancel',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                "immediate" => $request->input('immediateValue'),
                "reason" => $request->input('reasonValue')
              ]
      ];
      self::sendRequest($data);
    }

    public function serviceLabel(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/'. $request->input('id') . '/label',
      ];
      self::sendRequest($data);
    }

    public function changeServiceLabel(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/'. $request->input('id') . '/label',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                  "label" => $request->input("labelValue"),
              ]
      ];
      self::sendRequest($data);
    }

    public function serviceDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/'. $request->input('id'),

      ];
      self::sendRequest($data);
    }

    public function upgradeRequest(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/'. $request->input('id') . '/upgrade',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                "resources" => $request->input("resourcesValue"),
                "package" => $request->input("packageValue"),
                "cycle" => $request->input("cycleValue"),
                "send" => $request->input("sendValue")
              ]

      ];
      self::sendRequest($data);
    }

    public function getServiceIdFromOrderNumber(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/order'. $request->input('order_num'),
      ];
      self::sendRequest($data);
    }
    //END SERVICES

    //START CART
    public function listProductCategories(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/category',
      ];
      self::sendRequest($data);
    }

    public function listProductsInCategory(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/category/' . $request->input('category_id') . '/product',
      ];
      self::sendRequest($data);
    }


    public function getProductConfigurationDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/order/' . $request->input('product_id'),
      ];
      self::sendRequest($data);
    }

    public function orderMultipleServices(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/order',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                "pay_method" => $request->input('pay_method'),
                "items" => [
                    [
                        "type" =>  $request->input("product"),
                        "product_id" =>  $request->input("product_id"),
                        "domain" =>  $request->input("domain"),
                        "cycle" => $request->input("cycle")
                    ],
                    [
                        "type" => $request->input("certificate"),
                        "product_id" => $request->input("product_id"),
                        "csr" => $request->input("csr"),
                        "years" => $request->input("years"),
                        "approver_email" => $request->input("approver_email")
                    ]
                ]
            ]
      ];
      self::sendRequest($data);
    }

    public function getOrderQuote(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/quote',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                "pay_method" => $request->input('pay_methodValue'),
                "items" =>  $request->input("itemsValue")
            ]
      ];
      self::sendRequest($data);
    }

    public function orderNewService(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/order/' . $request->input('product_id'),
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                "domain" => $request->input("domainValue"),
                "cycle" => $request->input("cycleValue"),
                "pay_method" => $request->input("pay_methodValue"),
                "custom" => $request->input("customValue"),
                "promocode" => $request->input("promocodeValue")
            ]
      ];
      self::sendRequest($data);
    }

    public function getAvailableVPSProducts(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/category/available/vps',
      ];
      self::sendRequest($data);
    }

    public function orderAddon(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/' . $request->input('id') . '/addon',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                "addon_id" => $request->input("addon_idValue"),
              ]
      ];
      self::sendRequest($data);
    }

    public function listAllAddons(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/' . $request->input('id') . '/addon',
      ];
      self::sendRequest($data);
    }
    // END CART

    //START DNS
    public function listDNS(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/dns',
      ];
      self::sendRequest($data);
    }

    public function addDNSZone(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/' . $request->input('service_id') . '/dns',
              'postFields' =>  [
                  "name" => $request->input("nameValue")
              ]
      ];
      self::sendRequest($data);
    }

    public function getDNSDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/' . $request->input('service_id') . '/dns/' . $request->input('zone_id'),
      ];
      self::sendRequest($data);
    }

    public function removeDNSZone(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/' . $request->input('service_id') . '/dns/' . $request->input('zone_id'),
              'requestMethod' => 'CURLOPT_DELETE',
      ];
      self::sendRequest($data);
    }

    public function addDNSRecord(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/' . $request->input('service_id') . '/dns/' . $request->input('zone_id') . '/records',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                 "name" => $request->input("nameValue"),
                 "ttl" => $request->input("ttlValue"),
                 "priority" => $request->input("priorityValue"),
                 "type" => $request->input("typeValue"),
                 "content" => $request->input("contentValue")
              ]
      ];
      self::sendRequest($data);
    }

    public function editDNSRecord(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/' . $request->input('service_id') . '/dns/' . $request->input('zone_id') . '/records/' . $request->input('record_id'),
              'requestMethod' => 'CURLOPT_PUT',
              'postFields' => [
                 "name" => $request->input("nameValue"),
                 "ttl" => $request->input("ttlValue"),
                 "priority" => $request->input("priorityValue"),
                 "type" => $request->input("typeValue"),
                 "content" => $request->input("contentValue")
              ]
      ];
      self::sendRequest($data);
    }

    public function removeDNSRecord(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/service/' . $request->input('service_id') . '/dns/' . $request->input('zone_id') . '/records/' . $request->input('record_id'),
              'requestMethod' => 'CURLOPT_DELETE',
      ];
      self::sendRequest($data);
    }
    // END DNS

    // START SSL Certificates
    public function listSSLCertificates(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/certificate',
      ];
      self::sendRequest($data);
    }

    public function certificateDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/certificate/' . $request->input('id'),
      ];
      self::sendRequest($data);
    }

    public function downloadCertificate(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/certificate/' . $request->input('id') . '/crt',
      ];
      self::sendRequest($data);
    }

    public function listAvailableCertificates(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/certificate/order',
      ];
      self::sendRequest($data);
    }

    public function listServerSoftwareForCertificates(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/certificate/order/' . $request->input('product_id') . '/software',
      ];
      self::sendRequest($data);
    }

    public function orderNewCertificates(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/certificate/order',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                "product_id" => $request->input("product_idValue"),
                "csr" => $request->input("csrValue"),
                "years" => $request->input("yearsValue"),
                "pay_method" => $request->input("pay_methodValue"),
                "approver_email" => $request->input("approver_emailValue"),
                "admin" => $request->input("adminValue"),
                "tech" => $request->input("techValue"),
                "billing" => $request->input("billingValue"),
                "organization" => $request->input("organizationValue"),
                "software" => $request->input("softwareValue"),
                "data" => $request->input("dataValue")
              ]
      ];
      self::sendRequest($data);
    }
    // END SSL Certificates

    // START Server Management
    public function listAllServers(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server',
      ];
      self::sendRequest($data);
    }

    public function serverDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id'),
      ];
      self::sendRequest($data);
    }

    public function taskResult(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/task/' . $request->input('task_id'),
      ];
      self::sendRequest($data);
    }

    public function reboot(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/reboot',
              'requestMethod' => 'CURLOPT_POST',
      ];
      self::sendRequest($data);
    }

    public function reinstall(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/reinstall',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                 "os" =>  $request->input("osValue"),
                 "script" =>  $request->input("scriptValue"),
                 "ssh_key" =>  $request->input("ssh_keyValue")
              ]
      ];
      self::sendRequest($data);
    }

    public function resetPasswordServer(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/resetpassword',
      ];
      self::sendRequest($data);
    }

    public function launchWebConsole(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/webconsole',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' =>[
                 "timeout" => $request->input("timeoutValue"),
                 "whitelabel" => $request->input("whitelabelValue")
              ]
      ];
      self::sendRequest($data);
    }

    public function changeHostname(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/rename',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' =>[
                 "hostname" => $request->input("hostnameValue"),
              ]
      ];
      self::sendRequest($data);
    }

    public function changePTRRecord(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/changeptr',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' =>[
                "ip_address" => $request->input("ip_addressValue"),
                "hostname" => $request->input("hostnameValue")
              ]
      ];
      self::sendRequest($data);
    }

    public function flushIPTables(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/flushfirewall',
      ];
      self::sendRequest($data);
    }

    public function changeDNSServers(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/changedns',
              'requestMethod' => 'CURLOPT_POST',
              'postFields' => [
                "ns1" => $request->input("ns1Value"),
                "ns2" => $request->input("ns2Value"),
                "ns3" => $request->input("ns3Value"),
                "ns4" => $request->input("ns4Value")
              ]
      ];
      self::sendRequest($data);
    }

    public function availableOSList(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/oses',
      ];
      self::sendRequest($data);
    }

    public function getAdditionalIPs(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/ips',
      ];
      self::sendRequest($data);
    }

    public function getUsageGraphs(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/ips/graphs/' . $request->input('width'),
      ];
    self::sendRequest($data);
    }

    public function getUsageHistoryServer(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/server/' . $request->input('server_id') . '/history',
      ];
      self::sendRequest($data);
    }
    // END Server Management

    // START Init Script Management
    public function getAvailableInitScripts(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/scripts',
      ];
      self::sendRequest($data);
    }

    public function getInitScriptByID(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/scripts/' . $request->input('id'),
      ];
      self::sendRequest($data);
    }
    // END Init Script Management

    // START VPN Management
    public function listVPNServers(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/vpn/servers',
      ];
      self::sendRequest($data);
    }

    public function listVPNClients(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/vpn/clients',
      ];
      self::sendRequest($data);
    }

    public function vpnConfigTemplate(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/vpn/servers/' . $request->input('id') . '/config/' . $request->input('template'),
      ];
      self::sendRequest($data);
    }

    public function loginDetails(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/vpn/' . $request->input('id') . '/details',
      ];
      self::sendRequest($data);
    }

    public function resetPasswordVPN(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/vpn/' . $request->input('id') . '/resetpassword',
              'requestMethod' => 'CURLOPT_POST',
      ];
      self::sendRequest($data);
    }

    public function getUsageHistoryVPN(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/vpn/' . $request->input('id') . '/history',
      ];
      self::sendRequest($data);
    }

    public function getActiveVPNSessions(Request $request)
    {
      $data = [
              'url' => 'https://billing.time4vps.com/api/vpn/' . $request->input('id') . '/sessions',
      ];
      self::sendRequest($data);
    }

}
